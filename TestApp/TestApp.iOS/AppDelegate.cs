﻿using Foundation;
using MvvmCross.Platforms.Ios.Core;
using UIKit;

namespace TestApp.IOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            return base.FinishedLaunching(app, options);
        }
    }
}