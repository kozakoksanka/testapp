﻿using MvvmCross.Platforms.Ios.Core;
using MvvmCross.ViewModels;

namespace TestApp.IOS
{
    public class Setup : MvxIosSetup
    {
        protected override IMvxApplication CreateApp()
        {
            return new App();
        }
    }
}