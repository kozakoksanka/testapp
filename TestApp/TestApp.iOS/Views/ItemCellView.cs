﻿using System;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using TestApp.Data.Views;
using UIKit;

namespace TestApp.IOS.Views
{
    public partial class ItemCellView : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("ItemCellView");
        public static readonly UINib Nib;

        static ItemCellView()
        {
            Nib = UINib.FromName("ItemCellView", NSBundle.MainBundle);
        }

        protected ItemCellView(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<ItemCellView, ItemView>();
                set.Bind(title_label).To(m => m.Title);
                set.Bind(subtitle_label).To(m => m.Subtitle);
                set.Bind(description_label).To(m => m.Description);           
                set.Apply();
            });
        }
    }
}