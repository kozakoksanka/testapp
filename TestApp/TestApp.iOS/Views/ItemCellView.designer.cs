﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestApp.IOS.Views
{
    [Register ("ItemCellView")]
    partial class ItemCellView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel description_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel subtitle_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel title_label { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (description_label != null) {
                description_label.Dispose ();
                description_label = null;
            }

            if (subtitle_label != null) {
                subtitle_label.Dispose ();
                subtitle_label = null;
            }

            if (title_label != null) {
                title_label.Dispose ();
                title_label = null;
            }
        }
    }
}