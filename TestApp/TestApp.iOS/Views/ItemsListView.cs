﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using TestApp.IOS.Views;
using TestApp.ViewModels;
using UIKit;

namespace TestApp.iOS.Views
{

    public partial class ItemsListView : MvxViewController<ItemsListViewModel>
    {
        public ItemsListView() : base("ItemsListView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationController.NavigationBar.Hidden = true;
            var set = this.CreateBindingSet<ItemsListView, ItemsListViewModel>();

            set.Bind(username_label).For(v => v.Text).To(vm => vm.User.Username);
            set.Bind(password_label).For(v => v.Text).To(vm => vm.User.Password);

            set.Bind(user_info_stacklview).For(v => v.Hidden).To(vm => vm.IsItemListHidden);
            set.Bind(edit_list_stacklview).For(v => v.Hidden).To(vm => vm.IsEditItemsListHidden);
            set.Bind(logout_button).For(v => v.Hidden).To(vm => vm.IsLogoutViewHidden);

            set.Bind(item_list_button).To(vm => vm.ShowUserInfoCommand);
            set.Bind(edit_item_list_button).To(vm => vm.ShowEditItemsListCommand);
            set.Bind(logout_tap_button).To(vm => vm.ShowLogoutViewCommand);

            set.Bind(logout_button).To(vm => vm.LogoutCommand);
            set.Bind(add_item_button).To(vm => vm.AddItemCommand);
            set.Bind(remove_item_button).To(vm => vm.DeleteItemCommand);

            var source = new MvxSimpleTableViewSource(tableview, ItemCellView.Key, ItemCellView.Key);
            tableview.Source = source;
            tableview.RowHeight = 120;
            tableview.AllowsSelection = false;
            set.Bind(source).For(v => v.ItemsSource).To(vm => vm.ItemViews);
         
            set.Apply();
            tableview.ReloadData();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

