﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestApp.iOS.Views
{
    [Register ("ItemsListView")]
    partial class ItemsListView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton add_item_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton edit_item_list_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView edit_list_stacklview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton item_list_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton logout_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton logout_tap_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel password_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton remove_item_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView user_info_stacklview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel username_label { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (add_item_button != null) {
                add_item_button.Dispose ();
                add_item_button = null;
            }

            if (edit_item_list_button != null) {
                edit_item_list_button.Dispose ();
                edit_item_list_button = null;
            }

            if (edit_list_stacklview != null) {
                edit_list_stacklview.Dispose ();
                edit_list_stacklview = null;
            }

            if (item_list_button != null) {
                item_list_button.Dispose ();
                item_list_button = null;
            }

            if (logout_button != null) {
                logout_button.Dispose ();
                logout_button = null;
            }

            if (logout_tap_button != null) {
                logout_tap_button.Dispose ();
                logout_tap_button = null;
            }

            if (password_label != null) {
                password_label.Dispose ();
                password_label = null;
            }

            if (remove_item_button != null) {
                remove_item_button.Dispose ();
                remove_item_button = null;
            }

            if (tableview != null) {
                tableview.Dispose ();
                tableview = null;
            }

            if (user_info_stacklview != null) {
                user_info_stacklview.Dispose ();
                user_info_stacklview = null;
            }

            if (username_label != null) {
                username_label.Dispose ();
                username_label = null;
            }
        }
    }
}