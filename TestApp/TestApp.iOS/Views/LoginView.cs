﻿using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using TestApp.ViewModels;
using MvvmCross.Binding.BindingContext;

namespace TestApp.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class LoginView : MvxViewController<LoginViewModel>
    {
        public LoginView() : base("LoginView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            base.NavigationController.NavigationBarHidden = true;

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind(username_text_field).For(v => v.Text).To(vm => vm.User.Username);
            set.Bind(password_text_field).For(v => v.Text).To(vm => vm.User.Password);

            set.Bind(username_error_label).For(v => v.Text).To(vm => vm.User.UsernameErrorMessage);
            set.Bind(password_error_label).For(v => v.Text).To(vm => vm.User.PasswordErrorMessage);
            set.Bind(common_error_label).For(v => v.Text).To(vm => vm.ErrorMessage);

            set.Bind(login_button).To(vm => vm.LoginCommand);
            set.Apply();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
