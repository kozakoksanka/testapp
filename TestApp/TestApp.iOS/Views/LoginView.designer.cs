﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestApp.iOS.Views
{
    [Register ("LoginView")]
    partial class LoginView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel common_error_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton login_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel password_error_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField password_text_field { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel username_error_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField username_text_field { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (common_error_label != null) {
                common_error_label.Dispose ();
                common_error_label = null;
            }

            if (login_button != null) {
                login_button.Dispose ();
                login_button = null;
            }

            if (password_error_label != null) {
                password_error_label.Dispose ();
                password_error_label = null;
            }

            if (password_text_field != null) {
                password_text_field.Dispose ();
                password_text_field = null;
            }

            if (username_error_label != null) {
                username_error_label.Dispose ();
                username_error_label = null;
            }

            if (username_text_field != null) {
                username_text_field.Dispose ();
                username_text_field = null;
            }
        }
    }
}