﻿using System;
using System.IO;
using MvvmCross;
using MvvmCross.ViewModels;
using TestApp.Data;
using TestApp.Data.Entities;
using TestApp.Data.Repositories;
using TestApp.Data.Services;
using TestApp.Data.Services.Interfaces;
using TestApp.Services;
using TestApp.Services.Interfaces;
using TestApp.ViewModels;

namespace TestApp
{
    public class App: MvxApplication
    {
        static Database database;

        public static Database Database
        {
            get
            {
                if (database == null)
                {
                    database = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TestApp.db3"));
                }

                return database;
            }
        }

        public override void Initialize()
        {
            base.Initialize();
            Mvx.IoCProvider.RegisterType<IRepository<User>, UserRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<Item>, ItemRepository>();
            Mvx.IoCProvider.RegisterType<IUserService, UserService>();
            Mvx.IoCProvider.RegisterType<IItemService, ItemService>();
            Mvx.IoCProvider.RegisterType<ILoginService, LoginService>();
      
            RegisterAppStart<LoginViewModel>();
        }
    }
}
