﻿using System.Threading.Tasks;
using SQLite;
using TestApp.Data.Entities;

namespace TestApp.Data
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;
      
        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            CreateTables();
            MockUserData();
        }
           
        public Task<TEntity[]> GetItemsAsync<TEntity>() where TEntity : BaseEntity, new()
        {
            return _database.Table<TEntity>().ToArrayAsync();
        }

        public Task<TEntity> GetItemAsync<TEntity>(int id) where TEntity : BaseEntity, new()
        {
            return _database.Table<TEntity>()
                            .Where(i => i.Id == id)
                            .FirstOrDefaultAsync();
        }
       

        public async Task<TEntity> SaveItemsAsync<TEntity>(TEntity item) where TEntity : BaseEntity, new()
        {
            if (item.Id != 0)
            {
                await _database.UpdateAsync(item);
            }
            else
            {
                await _database.InsertAsync(item);
            }

            return item;
        }

        public Task<int> DeleteItemAsync<TEntity>(TEntity item) where TEntity : BaseEntity, new()
        {
            return _database.DeleteAsync(item);
        }


        private void CreateTables()
        {
            _database.CreateTableAsync<User>().Wait();
            _database.CreateTableAsync<Item>().Wait();
        }

        public void MockUserData()
        {
            _database.InsertAsync(new User {Username = "Demo", Password = "Demo" });
        }
    }
}
