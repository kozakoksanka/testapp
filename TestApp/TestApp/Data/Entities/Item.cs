﻿using TestApp.Utils;

namespace TestApp.Data.Entities
{
    public class Item: BaseEntity
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }


        public Item (int userId)
        {
            Title = StringUtils.RandomString(20);
            Subtitle = StringUtils.RandomString(30);
            Description = StringUtils.RandomString(100);
            UserId = userId;
        }

        public Item()
        {
        }
    }
}
