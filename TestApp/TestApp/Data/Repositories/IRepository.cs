﻿using System.Threading.Tasks;
using TestApp.Data.Entities;

namespace TestApp.Data.Repositories
{
    public interface IRepository <TEntity> where TEntity: BaseEntity
    {
        Task<TEntity[]> GetItemsAsync();
        Task<TEntity> GetItemAsync(int id);
        Task<int> SaveItemsAsync(TEntity item);
        Task<int> DeleteItemAsync(TEntity item);
    }
}