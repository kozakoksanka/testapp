﻿using System.Threading.Tasks;
using TestApp.Data.Entities;

namespace TestApp.Data.Repositories
{
    public class ItemRepository : IRepository<Item>
    {
        public Task<int> DeleteItemAsync(Item item)
        {
            return App.Database.DeleteItemAsync(item);
        }

        public Task<Item> GetItemAsync(int id)
        {
            return App.Database.GetItemAsync<Item>(id);
        }

        public Task<Item[]> GetItemsAsync()
        {
            return App.Database.GetItemsAsync<Item>();
        }

        public async Task<int> SaveItemsAsync(Item item)
        {
           var savedItem = await App.Database.SaveItemsAsync(item);
           return savedItem.Id;
        }
    }
}
