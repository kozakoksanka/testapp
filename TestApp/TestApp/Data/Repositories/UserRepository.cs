﻿using System.Threading.Tasks;
using TestApp.Data.Entities;

namespace TestApp.Data.Repositories
{
    public class UserRepository : IRepository<User> 
    {
        public Task<int> DeleteItemAsync(User item)
        {
           return App.Database.DeleteItemAsync(item);
        }

        public Task<User> GetItemAsync(int id) 
        {
            return App.Database.GetItemAsync<User>(id);
        }

        public Task<User[]> GetItemsAsync()
        {
            return App.Database.GetItemsAsync<User>();
        }

        public async Task<int> SaveItemsAsync(User item) 
        {
            var user = await App.Database.SaveItemsAsync(item);
            return user.Id;
        }
    }
}