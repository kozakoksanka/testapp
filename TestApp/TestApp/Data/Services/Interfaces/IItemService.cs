﻿using System.Threading.Tasks;
using TestApp.Data.Entities;
using TestApp.Data.Views;

namespace TestApp.Data.Services.Interfaces
{
    public interface IItemService
    {
        Task<ItemView[]> GetItemsByUserAsync(int userId);
        Task<ItemView> AddItemAsync(Item item);
        Task<bool> DeleteItemAsync(int itemId);
    }
}
