﻿using System.Threading.Tasks;

namespace TestApp.Data.Services.Interfaces
{
    public interface IUserService
    {
        Task<int> GetUserIdAsync(string username, string password);
    }
}
