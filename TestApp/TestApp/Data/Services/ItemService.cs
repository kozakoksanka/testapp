﻿using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using TestApp.Data.Entities;
using TestApp.Data.Repositories;
using TestApp.Data.Services.Interfaces;
using TestApp.Data.Views;

namespace TestApp.Data.Services
{
    public class ItemService : IItemService
    {
        private readonly IRepository<Item> _repository;

        public ItemService(IRepository<Item> repository)
        {
            _repository = repository;
        }

        public async Task<ItemView[]> GetItemsByUserAsync(int userId)
        {
            var items = await _repository.GetItemsAsync();
            return items
                .Where (item=> item.UserId == userId)
                .Select(item => new ItemView(item.Id, item.Title,item.Subtitle,item.Description))
                .ToArray();
        }

        public async Task<ItemView> AddItemAsync(Item item)
        {
            var id = await _repository.SaveItemsAsync(item);
            return new ItemView(id, item.Title, item.Subtitle, item.Description);
        }

        public async Task<bool> DeleteItemAsync(int itemId)
        {
            var item = await _repository.GetItemAsync(itemId);
            var deletedItemId = await _repository.DeleteItemAsync(item);

            return deletedItemId != default(int);
        }
    }
}