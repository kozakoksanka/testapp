﻿using System.Linq;
using System.Threading.Tasks;
using TestApp.Data.Entities;
using TestApp.Data.Repositories;
using TestApp.Data.Services.Interfaces;

namespace TestApp.Data.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _repository;
        public UserService(IRepository<User> repository)
        {
            _repository = repository;
        }

        public async Task<int> GetUserIdAsync(string username, string password)
        {
            var users = await _repository.GetItemsAsync();
            var user = users.FirstOrDefault(item => item.Username == username && item.Password == password);

            return user == null ? default(int) : user.Id;
        }
    }
}

