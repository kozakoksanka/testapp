﻿using MvvmCross.Plugin.FieldBinding;
using MvvmCross.ViewModels;

namespace TestApp.Data.Views
{
    public class ItemView: MvxViewModel
    {
        public INC<int> Id = new NC<int>();
        public INC<string> Title = new NC<string>();
        public INC<string> Subtitle = new NC<string>();
        public INC<string> Description = new NC<string>();

        public ItemView(
            int id,
            string title, 
            string subtitle,
            string description)
        {
            Id.Value = id;
            Title.Value = title;
            Subtitle.Value = subtitle;
            Description.Value = description;
        }
    }
}
