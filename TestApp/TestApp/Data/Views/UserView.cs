﻿using MvvmCross.Plugin.FieldBinding;
using MvvmCross.ViewModels;

namespace TestApp.Data.Views
{
    public class UserView : MvxViewModel
    {
        private const string UsernameError = "Username is required";
        private const string PasswordError = "Password is required";

        public int Id { get; set; }

        public INC<string> Username = new NC<string>();
        public INC<string> UsernameErrorMessage = new NC<string>();
        public INC<string> Password = new NC<string>();
        public INC<string> PasswordErrorMessage = new NC<string>();

        public UserView(string username, string password)
        {
            Username.Value = username;
            Password.Value = password;
        }

        public UserView()
        {
        }

        public bool Validate()
        {
            var isValid = true;

            if (string.IsNullOrWhiteSpace(Username.Value))
            {
                UsernameErrorMessage.Value = UsernameError;
                isValid = false;
            }
            else
            { 
                ClearErrorMessage(UsernameErrorMessage);

            }

            if (string.IsNullOrWhiteSpace(Password.Value))
            {
                PasswordErrorMessage.Value = PasswordError;
                isValid = false;
            }
            else
            {
                ClearErrorMessage(PasswordErrorMessage);
            }

            return isValid;
        }

        public void ClearErrorMessage(INC<string> field)
        {
            field.Value = string.Empty;
        }
    }
}
