﻿using System.Threading.Tasks;

namespace TestApp.Services.Interfaces
{
    public interface ILoginService
    {
        Task<int> LoginAsync(string username, string password);
        Task<bool> LogoutAsync(string username, string password);
    }
}
