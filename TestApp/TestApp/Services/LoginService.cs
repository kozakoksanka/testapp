﻿using System;
using System.Threading.Tasks;
using TestApp.Data.Services;
using TestApp.Data.Services.Interfaces;
using TestApp.Services.Interfaces;

namespace TestApp.Services
{
    public class LoginService: ILoginService
    {
        private readonly IUserService _userService;
        public LoginService(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<int> LoginAsync(string username, string password)
        {
            return await _userService.GetUserIdAsync(username, password);
        }

        public async Task<bool> LogoutAsync(string username, string password)
        {
            // Nothing to do

            return true;
        }
    }
}
