﻿using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.FieldBinding;
using MvvmCross.ViewModels;
using TestApp.Data.Entities;
using TestApp.Data.Services.Interfaces;
using TestApp.Data.Views;

namespace TestApp.ViewModels
{
    public class ItemsListViewModel: MvxViewModel<UserView>
    { 
        private readonly IMvxNavigationService _mvxNavigation;
        private readonly IItemService _itemService;

        public UserView User { get; set; }

        public INC<bool> IsItemListHidden = new NC<bool>();
        public INC<bool> IsEditItemsListHidden = new NC<bool>();
        public INC<bool> IsLogoutViewHidden = new NC<bool>();

        private MvxObservableCollection<ItemView> _itemViews;
        public MvxObservableCollection<ItemView> ItemViews
        {
            get => _itemViews;
            set => SetProperty(ref _itemViews, value);
        }

        private MvxCommand _showUserInfoCommand;
        public ICommand ShowUserInfoCommand => _showUserInfoCommand =
            _showUserInfoCommand ?? new MvxCommand(() => SetHiddenProperties(false, true, true));


        private MvxCommand _showEditItemsListCommand;
        public ICommand ShowEditItemsListCommand => _showEditItemsListCommand =
            _showEditItemsListCommand ?? new MvxCommand(() => SetHiddenProperties(true, false, true));
          

        private MvxCommand _showLogoutViewCommand;
        public ICommand ShowLogoutViewCommand => _showLogoutViewCommand =
            _showLogoutViewCommand ?? new MvxCommand(() => SetHiddenProperties(true, true, false));
         

        private MvxCommand _logoutCommand;
        public ICommand LogoutCommand => _logoutCommand =
            _logoutCommand ?? new MvxCommand(async() =>
            {
                await _mvxNavigation.Navigate<LoginViewModel>();
            });

        private MvxCommand _addItemCommand;
        public ICommand AddItemCommand => _addItemCommand =
            _addItemCommand ?? new MvxCommand(async () =>
            {
                var addedItem = await _itemService.AddItemAsync(new Item(User.Id));
                ItemViews.Add(addedItem);
            });

        private MvxCommand _deleteItemCommand;
        public ICommand DeleteItemCommand => _deleteItemCommand =
            _deleteItemCommand ?? new MvxCommand(async () =>
            {
                var itemToDelete = ItemViews[ItemViews.Count - 1];
                await _itemService.DeleteItemAsync(itemToDelete.Id.Value);
                ItemViews.Remove(itemToDelete);
            }, ()=> ItemViews.Count >= 1);


        public ItemsListViewModel(IMvxNavigationService mvxNavigation, IItemService itemService)
        {
            _mvxNavigation = mvxNavigation;
            _itemService = itemService;
        }

        public override void Prepare(UserView parameter)
        {
            User = parameter;

        }

        public override async Task Initialize()
        {
            await base.Initialize();

            SetHiddenProperties(false, true, true);
            await LoadItemsAsync();
        }

        private void SetHiddenProperties(
            bool isItemListHidden, 
            bool iIsEditItemsListHidden, 
            bool isLogoutViewHidden)
        {
            IsItemListHidden.Value = isItemListHidden;
            IsEditItemsListHidden.Value = iIsEditItemsListHidden;
            IsLogoutViewHidden.Value = isLogoutViewHidden;
        }

        private async Task LoadItemsAsync()
        {
            var items = await _itemService.GetItemsByUserAsync(User.Id);
            ItemViews = new MvxObservableCollection<ItemView>(items);
        }
    }
}