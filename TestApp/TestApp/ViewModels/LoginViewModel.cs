﻿using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.FieldBinding;
using MvvmCross.ViewModels;
using TestApp.Data.Views;
using TestApp.Services.Interfaces;

namespace TestApp.ViewModels
{

    public class LoginViewModel: MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigation;
        private readonly ILoginService _loginService;

        private const string UserNotFoundErrorMessage = "User was not found";

        public UserView User { get; set; }
        public INC<string> ErrorMessage = new NC<string>();

        public LoginViewModel(
            IMvxNavigationService mvxNavigation, 
            ILoginService loginService)
        {
            _mvxNavigation = mvxNavigation;
            _loginService = loginService;
        }

        public override async Task Initialize()
        {
            User = new UserView();
            await base.Initialize();
        }


        private MvxCommand _loginCommand;
        public ICommand LoginCommand => _loginCommand = 
            _loginCommand ?? new MvxCommand(async () => await LoginAsync());


        private async Task LoginAsync()
        {
            var isValid = User.Validate();
            if (!isValid)
            {
                ErrorMessage.Value = string.Empty;
                return;
            }

            User.ClearErrorMessage(User.UsernameErrorMessage);
            User.ClearErrorMessage(User.PasswordErrorMessage);
   
            var userId = await _loginService.LoginAsync(
                User.Username.Value, 
                User.Password.Value);

            if (userId == default(int))
            {
                ErrorMessage.Value = UserNotFoundErrorMessage;
                return;
            }

            User.Id = userId;
            await _mvxNavigation.Navigate<ItemsListViewModel, UserView>(User);
        }
    }
}
